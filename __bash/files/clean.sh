#!/bin/bash
##############################################################
#   SCRIPT FOR CLEANING DIRECTORIES
##############################################################
#
# commands to clean files and directories
#
##############################################################


# REMOVE FILES WITH EXTENTION
###################################
function CLEAN_EXT(){

    local SEARCH_PATH=$1
    local NAME_TYPE=$2

    echo ""
    echo "$NAME_TYPE $SEARCH_PATH"


    #search for names of type $NAME
    for filename in $(find "$SEARCH_PATH" -type f  -name "*.$NAME_TYPE" )
    do

        echo "    --->  $filename"

        #remove
        rm $filename

    done
    echo "--"
}

# REMOVE DIRECTORIES WITH NAME
###################################

function CLEAN_DIRS(){

    local SEARCH_PATH=$1
    local NAME=$2

    echo ""
    echo "$NAME $SEARCH_PATH"


    #search for names of type $NAME
    for filename in $(find "$SEARCH_PATH" -type d  -name "$NAME" )
    do

        echo "    --->  $filename"

        #remove
        rm -r $filename

    done
    echo "--"
}

##############################################################

