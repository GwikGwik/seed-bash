DEBUG true
for elt in $(find $SEED_DIR -type d -name "__sphinx")
do
    TITLE "$(dirname $elt)"
    cd $(dirname $elt) && @sphinx run
done

#cd $SEED_DIR
#SPHINX_DIR
#SPHINX_PYTHON
#SPHINX_CHECK
#SPHINX_ADD
#SPHINX_CP "$SPHINX_APP/main/__etc/index.rst" "index.rst"
#SPHINX_CP "$SEED_DIR/main/__init__.png" "__init__.png"
#SPHINX_BUILD
