#!/bin/bash
PARAGRAPH "CHMOD BIN"
for directory in $(find $SEED_DIR -mindepth 1 -type d -name "__bin")
do

    for script in $(find $directory -mindepth 1 -type f)
    do
        MESSAGE "BIN $script"
        chmod +x $script
    done
done

PARAGRAPH "SYSTEM_CONFIG"
#SYSTEM_CONFIG
